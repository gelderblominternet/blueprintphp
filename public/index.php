<?php
// Register vendors
require_once realpath(__DIR__.'/../').'/vendor/autoload.php';

// Environment
$dotenv = new Dotenv\Dotenv(realpath(__DIR__.'/../'));
$dotenv->load();

// Blueprint namespace
use Gelderblominternet\Blueprintphp\Blueprint;

// Sub-domain
$host = explode('.', getenv('HTTP_HOST'));
$subdomain = 'www';
if (count($host) > 2 && $host[0] != 'www') {
    $subdomain = $host[0];
}
putenv('SUBDOMAIN='.$subdomain);

// Bootstrap
require_once getenv('BASE_DIR').'resources/bootstrap.php';

// Set-up routing
Blueprint::instance()->loadBundleRoutes();

// Dispatch
$dispatcher = new Phroute\Phroute\Dispatcher(Blueprint::instance()->getService('router')->getData());
$response = $dispatcher->dispatch(Blueprint::instance()->request->server->get('REQUEST_METHOD'), parse_url(Blueprint::instance()->request->server->get('REQUEST_URI'), PHP_URL_PATH));
$response = Blueprint::instance()->prepare($response);

// Send response
$response->send();