<?php
// Used namespaces
use Gelderblominternet\Blueprintphp\Blueprint;
use Monolog\ErrorHandler;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Symfony\Component\HttpFoundation\Session\Session;

// Monolog setup
$logErrors = new Logger('Logger');
if (getenv('ENVIRONMENT') == 'prod') {
    $logErrors->pushHandler(new StreamHandler(getenv('BASE_DIR').'var/logs/errors.log', Logger::WARNING));
} else {
    $logErrors->pushHandler(new StreamHandler(getenv('BASE_DIR').'var/logs/errors.log', Logger::DEBUG));
}
$handler = new ErrorHandler($logErrors);
$handler->registerErrorHandler([], false);
$handler->registerExceptionHandler();
$handler->registerFatalHandler();

// Set-up blueprint bundles
Blueprint::instance()->addBundles([ 'Documentation' ]);

// Set-up services
Blueprint::instance()->setService('session', new Session());
Blueprint::instance()->getService('session')->start();
Blueprint::instance()->setService('router', new \Phroute\Phroute\RouteCollector());
Blueprint::instance()->setService('database', new \Medoo\Medoo([
    'database_type' => getenv('DATABASE_TYPE'),
    'database_name' => getenv('DATABASE_NAME'),
    'server' => getenv('DATABASE_SERVER'),
    'username' => getenv('DATABASE_USERNAME'),
    'password' => getenv('DATABASE_PASSWORD')
]));
?>